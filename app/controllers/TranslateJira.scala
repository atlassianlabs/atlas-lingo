package controllers

import play.api.mvc.{ RequestHeader, Action, Controller }
import com.atlassian.connect.play.scala.controllers.ActionJwtValidator
import com.atlassian.connect.play.scala.store.DefaultDbConfiguration
import com.atlassian.connect.play.scala.PlayAcConfigured
import kadai.Invalid._
import kadai.Result._
import model._
import model.Formats._
import service.{ Translation, JiraService, GoogleTranslate, TranslationService }
import scalaz.{ EitherT, NonEmptyList, \/-, -\/ }
import scalaz.concurrent.{ Future => ZFuture }
import scalaz.syntax.show._
import scalaz.syntax.std.option._
import scalaz.syntax.id._
import scalaz.std.option._
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.i18n.Lang
import scalaz.-\/
import service.Translation
import model.Language
import scalaz.\/-
import model.Issue
import kadai.Invalid
import play.api.Logger
import java.text.DateFormat
import java.util.Locale

case class IssuePageVocab(project: String = "Project", issueKey: String = "Issue Key", summary: String = "Summary",
  description: String = "Description", details: String = "Details", comments: String = "Comments", addcomment: String = "Added a comment", close: String = "Close")

object TranslateJira extends Controller with ActionJwtValidator with DefaultDbConfiguration with PlayAcConfigured with JiraService {

  val ts: TranslationService = new GoogleTranslate(play.api.Play.current.configuration)

  def locale(implicit req: RequestHeader): Locale = {
    req.acceptLanguages.headOption map (_.toLocale) getOrElse Locale.getDefault
  }

  def languageAccepted(implicit req: RequestHeader): Option[Language] = {
    req.acceptLanguages.headOption map {
      case Lang("zh", "CN") => Language("zh-CN")
      case Lang("zh", "TW") => Language("zh-TW")
      case Lang(code, _) => Language(code)
    }
  }

  def landing(issueKey: String) = Action.async { implicit req =>
    jwtValidatedAsync { implicit token =>
      val sentence = "Translate this issue!"
      zfuture2Future {
        languageAccepted(req).some(lang =>
          (ts.translatePlaintext(sentence.wrapNel, lang) map {
            _.head
          }).run
        ).none(ZFuture.now(sentence.right))
      } map {
        case -\/(invalid) => BadRequest(invalid.shows)
        case \/-(translated) => Ok(views.html.landing(issueKey, translated))
      }

    }
  }

  def issue(issueKey: String) = Action.async { implicit req =>
    jwtValidatedAsync { implicit token =>
      zfuture2Future {
        val vocab = IssuePageVocab()
        (retrieveIssue(issueKey) flatMap {
          case i @ Issue(k, s, d, p, cs) =>
            languageAccepted(req).some(lang =>
              ts.translatePlaintext(NonEmptyList(s, d, p, vocab.project, vocab.issueKey, vocab.summary, vocab.description,
                vocab.details, vocab.comments, vocab.addcomment, vocab.close) :::> cs.map(_.body), lang) flatMap { nel =>
                (nel.head, nel.tail) match {
                  case (ss, dd :: pp :: vp :: vi :: vs :: vd :: vdd :: vc :: ac :: vcc :: css) =>
                    val ccss = cs.zip(css).map(cb => cb._1.copy(body = cb._2))
                    Logger.info(s"comment size: ${ccss.size}")
                    EitherT(ZFuture.now((Issue(k, ss, dd, pp, ccss), IssuePageVocab(vp, vi, vs, vd, vdd, vc, ac, vcc)).right))
                  case _ => EitherT(ZFuture.now("Translation result not matching the original.".invalidResult))
                }
              }
            ).none(EitherT(ZFuture.now((i, vocab).right)))

        }).run
      } map {
        case -\/(invalid) => BadRequest(invalid.shows)
        case \/-((issue, vocab)) =>
          val df = DateFormat.getDateInstance(DateFormat.FULL, locale)
          Ok(views.html.jira.issue(issue, vocab, df))
      }
    }
  }
}
