package controllers

import model.{ Language, PlainText, Text }
import play.api.mvc.{ Action, Controller }
import scala.concurrent.{ Promise, Future }
import scala.concurrent.ExecutionContext.Implicits.global
import scalaz.{ \/-, -\/, NonEmptyList }
import scalaz.concurrent.{ Future => ZFuture }
import scalaz.syntax.show._
import service.{ FutureResult, GoogleTranslate, TranslationService }
import kadai.Invalid._

object Test extends Controller {

  val ts: TranslationService = new GoogleTranslate(play.api.Play.current.configuration)

  def test() = Action.async { implicit request =>
    zfuture2Future {
      ts.translate(NonEmptyList(Text(PlainText, "Translate")), Language("fr"), Some(Language("en"))).run
    } map {
      case -\/(invalid) => BadRequest(invalid.shows)
      case \/-(tr) => Ok(tr.head.toString)
    }

  }
}
