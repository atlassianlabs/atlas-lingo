import scala.concurrent.{ Promise, Future }
import scalaz.concurrent.{ Future => ZFuture }

package object controllers {

  def zfuture2Future[A](f: ZFuture[A]): Future[A] = {
    val p: Promise[A] = Promise()
    f.runAsync(a => p.success(a))
    p.future
  }

}
