package model

case class Paragraph[F <: Format](language: Language, text: Text[F])
