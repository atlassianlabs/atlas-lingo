package model

sealed abstract class Format(val name: String)

case object PlainText extends Format("text")
case object Html extends Format("html")

case class Text[F <: Format](format: F, rawText: String)

object Formats {
  implicit class ToText(s: String) {
    def plainText: Text[PlainText.type] = Text(PlainText, s)
    def html: Text[Html.type] = Text(Html, s)
  }
}
