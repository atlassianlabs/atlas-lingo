package model

import java.util.Date

case class Issue(key: String, summary: String, description: String, projectName: String, comments: List[Comment])

case class Comment(id: String, body: String, author: JiraUser, date: Date)

case class JiraUser(name: String, displayName: String, avatar: String)