import com.atlassian.connect.play.scala.PlayAcConfigured
import com.atlassian.connect.play.scala.settings.AcGlobalSettings
import play.api.{ Logger, Application }

object Global extends AcGlobalSettings with PlayAcConfigured {

  override def onStart(app: Application) {
    super.onStart(app)
    Logger.info(s"${prettyName} has started.")
  }

  override def onStop(app: Application) {
    super.onStop(app)
    Logger.info(s"${prettyName} has stopped.")
  }

  private lazy val prettyName = s"${acConfig.pluginName} (${acConfig.pluginKey}})"
}

