package service

import com.atlassian.connect.play.scala.model.AcHostModel
import model.{ Format, Paragraph, Text, Language }
import model.Formats._
import scalaz.NonEmptyList

case class Translation[F <: Format](source: Paragraph[F], target: Paragraph[F])

trait TranslationService {

  def translate[F <: Format](
    q: NonEmptyList[Text[F]],
    target: Language,
    source: Option[Language] = None,
    acHost: Option[AcHostModel] = None): FutureResult[NonEmptyList[Translation[F]]]

  def translateSimple[F <: Format](
    q: NonEmptyList[String],
    target: Language,
    source: Option[Language] = None,
    acHost: Option[AcHostModel] = None)(f: String => Text[F]): FutureResult[NonEmptyList[String]] = {
    translate(q map (_.plainText), target, source, acHost) map (_ map {
      case Translation(_, Paragraph(_, Text(_, t))) => t
    })
  }

  def translatePlaintext(
    q: NonEmptyList[String],
    target: Language,
    source: Option[Language] = None,
    acHost: Option[AcHostModel] = None): FutureResult[NonEmptyList[String]] =
    translateSimple(q, target, source, acHost)(_.plainText)

  def translateHtml(
    q: NonEmptyList[String],
    target: Language,
    source: Option[Language] = None,
    acHost: Option[AcHostModel] = None): FutureResult[NonEmptyList[String]] =
    translateSimple(q, target, source, acHost)(_.html)

}
