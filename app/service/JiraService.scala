package service

import argonaut._, Argonaut._
import com.atlassian.connect.play.scala.auth.Token
import com.atlassian.connect.play.scala.auth.jwt.JwtSignedAcHostWS
import com.atlassian.connect.play.scala.util.AcHostWS
import kadai.Result._
import play.api.Logger
import play.api.libs.ws.Response
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scalaz.concurrent.{ Future => ZFuture }
import scalaz.syntax.id._
import scalaz.syntax.monad._
import model.{ JiraUser, Comment, Issue }
import scalaz.EitherT
import java.util.Date
import java.text.SimpleDateFormat

trait JiraService extends JwtSignedAcHostWS {

  def retrieveIssue(issueKey: String)(implicit token: Token): FutureResult[Issue] = {
    val request = AcHostWS.uri(s"/rest/api/2/issue/${issueKey}") // ?expand=renderedFields
    Logger.debug(s"Making request to ${request.url} with ${request.queryString}")
    future2ZFutureResult(request.signedGet()) flatMap { response =>
      (response.status match {
        case play.api.http.Status.OK =>
          Logger.info(s"200 OK from JIRA: ${response.body}")
          EitherT(ZFuture.now(response.body.decodeEither[Issue] leftMap (_.invalid)))
        case _ =>
          val msg = s"Error response from JIRA. Status code: ${response.status}. Status text: ${response.statusText}. Body: ${response.body}"
          Logger.info(msg)
          EitherT(ZFuture.now(msg.invalidResult))
      })
    }
  }

  case class WikiRenderRequest(issueKey: String, unrenderedMarkup: String, rendererType: String = "atlassian-wiki-renderer")

  implicit def IssueDecoder: DecodeJson[Issue] = DecodeJson { c =>
    for {
      key <- (c --\ "key").as[String]
      summary <- (c --\ "fields" --\ "summary").as[String]
      descHtml <- (c --\ "renderedFields" --\ "description").as[String] ||| (c --\ "fields" --\ "description").as[String]
      projectName <- (c --\ "fields" --\ "project" --\ "name").as[String]
      comments <- (c --\ "fields" --\ "comment" --\ "comments").as[List[Comment]]
    } yield Issue(key, summary, descHtml, projectName, comments)
  }

  val dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

  implicit def CommendDecoder: DecodeJson[Comment] = DecodeJson { c =>
    for {
      id <- (c --\ "id").as[String]
      body <- (c --\ "body").as[String]
      author <- (c --\ "author").as[JiraUser]
      tt <- (c --\ "created").as[String]
    } yield Comment(id, body, author, dateFormatter.parse(tt))
  }

  implicit def JiraUserDecoder: DecodeJson[JiraUser] = DecodeJson { c =>
    for {
      name <- (c --\ "name").as[String]
      displayName <- (c --\ "displayName").as[String]
      avatar <- (c --\ "avatarUrls" --\ "16x16").as[String]
    } yield JiraUser(name, displayName, avatar)
  }
}
