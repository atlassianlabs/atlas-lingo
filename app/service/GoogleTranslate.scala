package service

import argonaut._, Argonaut._
import com.atlassian.connect.play.scala.model.AcHostModel
import scala.concurrent.{ ExecutionContext, Future }
import ExecutionContext.Implicits.global
import java.net.URLEncoder.{ encode => urlEncode }
import kadai.Result._
import model._
import play.api.{ Logger, Configuration }
import play.api.libs.ws.WS
import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success }
import scalaz.{ EitherT, NonEmptyList }
import scalaz.std.option._
import scalaz.std.string._
import scalaz.syntax.monad._
import scalaz.syntax.id._
import scalaz.syntax.std.option._
import scalaz.concurrent.{ Future => ZFuture }
import com.atlassian.connect.play.scala.model.AcHostModel

class GoogleTranslate(config: Configuration) extends TranslationService {

  val translateUri = "https://www.googleapis.com/language/translate/v2"

  val defaultApiKey = config.getString("google.translate.api.key") |
    (throw new IllegalStateException("Google Translate API Key not found!"))

  val defaultApiKeyUrlEncoded = defaultApiKey.urlEncoded

  implicit class StringEncoder(val s: String) {
    def urlEncoded = urlEncode(s, "UTF-8")
  }

  private[service] def urlParam(name: String, ov: Option[String], append: String = "") =
    ~ov.map(v => s"${name.urlEncoded}=${v.urlEncoded}${append}")

  private[service] def convert[F <: Format](
    source: Option[Language], sourceText: NonEmptyList[Text[F]],
    target: Language, result: NonEmptyList[GoogleTranslateResult]): Result[NonEmptyList[Translation[F]]] = {

    (sourceText zip result).traverse1[Result, Translation[F]] {
      case (st: Text[F], r: GoogleTranslateResult) => (source, r) match {
        case (_, GoogleTranslateResult(rt, Some(lang))) => Translation(Paragraph(lang, st), Paragraph(target, Text(st.format, rt))).right
        case (Some(lang), GoogleTranslateResult(rt, _)) => Translation(Paragraph(lang, st), Paragraph(target, Text(st.format, rt))).right
        case _ => "Source language not provided and not detected.".invalidResult
      }
    }
  }

  def translate[F <: Format](q: NonEmptyList[Text[F]], target: Language, source: Option[Language] = None, acHost: Option[AcHostModel] = None) = {
    val qp = q.map(t => s"q=${t.rawText.urlEncoded}").list.mkString("&")
    val url = s"${translateUri}?${urlParam("source", source.map(_.code), "&")}target=${target.code}&format=${q.head.format.name}&key=${defaultApiKeyUrlEncoded}&${qp}"
    val result: FutureResult[NonEmptyList[GoogleTranslateResult]] = future2ZFutureResult(WS.url(url).get()) flatMap { response =>
      (response.status match {
        case play.api.http.Status.OK =>
          Logger.info(s"200 OK from Google: ${response.body}")
          EitherT(ZFuture.now((response.body.decodeEither[List[GoogleTranslateResult]]) flatMap (_ match {
            case Nil => s"Failed to parse translation result ${response.body}".left
            case head :: tail => NonEmptyList.nel(head, tail).right
          }) leftMap (_.invalid)))
        case _ =>
          val msg = s"Error response from Google. Status code: ${response.status}. Status text: ${response.statusText}. Body: ${response.body}"
          Logger.info(msg)
          EitherT(ZFuture.now(msg.invalidResult))
      })
    }
    (result) flatMap (gtr => EitherT(ZFuture.now(convert(source, q, target, gtr))))
  }

  case class GoogleTranslateResult(translatedText: String, detectedSourceLanguage: Option[Language])

  implicit def GoogleTranslateResultDecoder: DecodeJson[GoogleTranslateResult] =
    casecodec2(GoogleTranslateResult.apply, GoogleTranslateResult.unapply)("translatedText", "detectedSourceLanguage")

  implicit def GoogleTranslateResultsDecoder: DecodeJson[List[GoogleTranslateResult]] = DecodeJson { c =>
    (c --\ "data" --\ "translations").as[List[GoogleTranslateResult]](ListDecodeJson)
  }

  implicit def LanguageDecoder: CodecJson[Language] = CodecJson[Language](lang => jString(lang.code), c => c.as[String] map Language.apply)

}
