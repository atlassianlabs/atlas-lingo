import kadai.Invalid
import kadai.Result._
import scala.concurrent.{ ExecutionContext, Future }
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{ Failure, Success }
import scalaz.concurrent.{ Future => ZFuture }
import scalaz.EitherT
import scalaz.syntax.id._

package object service {

  type Result[A] = kadai.Result[A]
  type FutureResult[A] = EitherT[ZFuture, Invalid, A]

  def future2ZFutureResult[A](f: Future[A])(implicit ec: ExecutionContext): FutureResult[A] = {
    EitherT[ZFuture, Invalid, A](ZFuture.async { register =>
      f.onComplete {
        case Success(a) => register(a.right)
        case Failure(e) => register(e.invalidResult)
      }
    })
  }

}
