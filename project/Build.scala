import com.typesafe.sbt.SbtScalariform._
import sbt._
import sbt.Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "atlas-lingo"
  val appVersion      = "1.0-SNAPSHOT"

  val playDep = Seq(
    "com.typesafe.play" %% "play" % "2.2.1")

  lazy val AC_PLAY_SCALA_VERSION = "0.3.3"
  lazy val KADAI_VERSION = "1.5.1"
  lazy val SCALAZ_VERSION = "7.0.4"
  lazy val ARGONAUT_VERSION = "6.0.4"
  //lazy val SLICK_VERSION = "2.0.2"
  //lazy val POSTGRES_JDBC_VERSION = "9.3-1101-jdbc4"
  //lazy val LIQUIBASE_VERSION = "3.1.1"

  val libDependencies = Seq(
    // Add your project dependencies here,
    jdbc,
    cache,
    "org.scalaz"            %% "scalaz-core"        % SCALAZ_VERSION,
    "org.scalaz"            %% "scalaz-effect"      % SCALAZ_VERSION,
    "org.scalaz"            %% "scalaz-concurrent"  % SCALAZ_VERSION,
    "com.typesafe"          %% "play-plugins-redis" % "2.1-1-RC2-robinf-3",
    "com.atlassian.connect" %% "ac-play-scala"      % AC_PLAY_SCALA_VERSION,
    "io.argonaut"           %% "argonaut"           % ARGONAUT_VERSION,
    "io.kadai"              %% "kadai"              % KADAI_VERSION exclude("com.chuusai", "shapeless_2.10")
  )

  val sharedTestDependencies = Seq(
    "junit" % "junit" % "4.11" % "test",
    "org.specs2" %% "specs2" % "1.14" % "test",
    "org.mockito" % "mockito-all" % "1.9.5" % "test",
    "com.typesafe.akka" %% "akka-testkit" % "2.1.1" % "test" withSources ())

  val appDependencies = playDep ++ libDependencies

  // credentials required when accessing atlassian-proxy-internal
  credentials := Seq(Credentials(Path.userHome / ".ivy2" / ".credentials"))

  val customResolvers = Seq(
    Resolver.defaultLocal,
    "atlassian-proxy" at "https://m2proxy.atlassian.com/content/groups/internal/",
    "atlassian-proxy-public" at "https://m2proxy.atlassian.com/content/groups/public/",
    "atlassian-maven-public" at "https://maven.atlassian.com/content/groups/public/",
    "Local Maven Repository" at "file://" + Path.userHome + "/.m2/repository",
    "Robin's Maven Repository" at "http://rewbs.bitbucket.org/mavenrepo/releases",
    "org.sedis Maven Repository" at "http://pk11-scratch.googlecode.com/svn/trunk",
    Classpaths.typesafeReleases,
    DefaultMavenRepository,
    Resolver.mavenLocal
  )

  val main = play.Project(appName, appVersion, appDependencies)
    .settings(scalaVersion := "2.10.3")
    .settings(scalariformSettings: _*)
    .settings(resolvers ++= customResolvers)
    .settings(net.virtualvoid.sbt.graph.Plugin.graphSettings: _*)
    // Add your own project settings here

}
