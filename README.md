This is your new Play 2.1 application
=====================================

This file will be packaged with your application, when using `play dist`.

Developer Notes
===============

Download Redis:

    wget http://download.redis.io/releases/redis-2.8.6.tar.gz
    tar xzf redis-2.8.6.tar.gz
    cd redis-2.8.6
    make

Start Redis:

    src/redis-server

Start Redis client:

    src/redis-cli

Command to clean the Redis database from Redis client:

    > flushdb

Start JIRA:

    atlas-run-standalone --product jira --version 6.2-OD-10-004-WN -Dserver=localhost --bundled-plugins com.atlassian.plugins:atlassian-connect-plugin:1.0.0,com.atlassian.jwt:jwt-plugin:1.0.0,com.atlassian.bundles:json-schema-validator-atlassian-bundle:1.0-m0,com.atlassian.upm:atlassian-universal-plugin-manager-plugin:2.15 --jvmargs -Datlassian.upm.on.demand=true

Note that the following command to start JIRA does not work:

    atlas-run-standalone --product jira --version 6.3-OD-03-012    -Dserver=localhost --bundled-plugins com.atlassian.plugins:atlassian-connect-plugin:1.0.2,com.atlassian.jwt:jwt-plugin:1.0.0,com.atlassian.bundles:json-schema-validator-atlassian-bundle:1.0-m0 --jvmargs -Datlassian.upm.on.demand=true

To debug atlas-lingo:

    sbt -Dconfig.file=conf/development-h2.conf -jvm-debug 9999 "run 9000"

To run development in sbt:

    run -Dconfig.file=conf/development-h2.conf
